module Api
  module V1

    class BooksController < ApplicationController
      before_action :set_paramaters, only: [:index]
      before_action :set_book, only: [:show, :update, :destroy]

      # GET /books
      # GET /books?title=name
      # GET /books?genre=name
      # GET /books?author=name
      def index
        render json: @books
      end

      # GET /books/1
      def show
        render json: @book
      end

      # POST /books
      def create
        @book = Book.new(book_params)

        if @book.save
          render json: @book, status: :created, location: @book
        else
          render json: @book.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /books/1
      def update
        if @book.update(book_params)
          render json: @book
        else
          render json: @book.errors, status: :unprocessable_entity
        end
      end

      # DELETE /books/1
      def destroy
        @book.destroy
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_book
        @book = Book.find(params[:id])
      end

      # パラメータによる検索を指定する
      private
      def set_paramaters
        if !(params[:title].blank?)
          set_titles
        elsif !(params[:genre].blank?)
          set_genre
        elsif !(params[:author].blank?)
          set_author
        else
          @books = Book.all
        end
      end

      # タイトルパラメーター指定時の検索
      # 部分一致検索
      private
      def set_titles
        # 完全一致検索をコメントとして残す
        # if Book.find_by(title: params[:title])
        #   @books = Book.where(title: params[:title])
        # end

        @books = Book.where('title LIKE ?' , "%#{params[:title]}%")
      end

      # ジャンルパラメーター指定時の検索
      private
      def set_genre
        if Book.find_by(genre: params[:genre])
          @books = Book.where(genre: params[:genre])
        end
      end

      # 著者パラメーター指定時の検索
      private
      def set_author
        if Book.find_by(author: params[:author])
          @books = Book.where(author: params[:author])
        end
      end

      # Only allow a trusted parameter "white list" through.
      def book_params
        params.fetch(:book, {})
      end
    end

  end
end
