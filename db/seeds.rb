# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require "csv"

CSV.foreach('db/books.csv') do |row|
  Book.create(:title => row[0],:author => row[1],:release_date => row[2],:genre => row[3],:isbn_code => row[4],:c_code => row[5],:loan_number => row[6])
end
