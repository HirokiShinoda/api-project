class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :release_date
      t.string :genre
      t.string :isbn_code
      t.string :c_code
      t.string :loan_number

      t.timestamps
    end
  end
end
